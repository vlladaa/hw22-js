const field = document.createElement('table')
field.style.borderCollapse = 'collapse'
field.style.margin = '100px auto'
const colorBlack = document.querySelector('.active')

for (let i = 0; i < 30; i++){

  let tr = document.createElement('tr')

  for (let j =0; j < 30; j++){
    let td = document.createElement('td')
    td.style.width = '17px'
    td.style.height = '17px'
    td.style.border = '1px solid black'
    tr.appendChild(td)
  }

  field.appendChild(tr);
}

document.body.appendChild(field)

field.addEventListener('click', (event) => {
  event.target.classList.toggle('active')
  event.stopPropagation();
  }
)

window.addEventListener('click', (event) => {
  field.classList.toggle('active')
  event.stopPropagation();
})